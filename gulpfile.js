var gulp   = require('gulp'),
    watch  = require('gulp-watch'),
    less   = require('gulp-less'),
    uglify = require('gulp-uglify');

gulp.task('less', function() {
    return gulp.src('./src/css/less/*.less')
        .pipe(less())
        .pipe(gulp.dest('./src/css'));
});

gulp.task('watch', function() {
    gulp.watch('./src/css/less/*.less', gulp.series('less'));
});
