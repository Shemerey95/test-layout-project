//  Function for the function of the button "Menu" on mobile devices and tablets:

function showMenu() {
    document.querySelector('.header__nav').classList.toggle('visible');
}

document.querySelector('.header__menu_button').addEventListener('click', showMenu);
